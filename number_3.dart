import 'dart:ffi';

void main(List<String> args) {
  var anyApp = new App();
  anyApp.appName = 'Ambani Africa';
  anyApp.sector = 'Education';
  anyApp.developer = 'Unkown';
  anyApp.year = 2021;

  anyApp.getAppDetails();
}

class App {
  String? appName;
  String? sector;
  String? developer;
  int? year;
  void getAppDetails() {
    print(appName?.toUpperCase());
    print('Sector of the winning App is $sector');
    print('MTN Business App award winner for $year');
  }
}
